	global loader
	
	MAGIC_NUMBER equ 0x1BADB002
	FLAGS	     equ 0x0
	CHECKSUM     equ -MAGIC_NUMBER
	
	KERNEL_STACK_SIZE equ 4096
	
	section .bss
	align 4
	kernel_stack:
		resb KERNEL_STACK_SIZE
		
	section .text
	align 4
		dd MAGIC_NUMBER	
		dd FLAGS
		dd CHECKSUM
	loader:
		mov esp, kernel_stack + KERNEL_STACK_SIZE
		
		extern display
		push dword 5 		;this parameter causes number of iteration of prints
		push dword 0
		push dword ' '
		push dword 0
		push dword 0
		call display
		
	.loop:
		jmp .loop
