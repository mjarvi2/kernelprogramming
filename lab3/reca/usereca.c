#include <stdlib.h>
#include <stdio.h>
#include <argp.h>
#include <sys/wait.h>
#include <unistd.h>

int main(int argc, char** argv){
  char N[1];
  printf("Enter value for recaman sequence: ");
  scanf("%s", N);
  pid_t pid = fork();
  if(pid ==0){
    char *args[3] = {"./reca", N, NULL};
      execvp("./reca", args);
     return 0;
  }
  wait(NULL);
  return 0;
}

