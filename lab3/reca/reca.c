#include <stdlib.h>
#include <stdio.h>
#include <argp.h>
#include <sys/wait.h>
#include <unistd.h>

void reca(int n){
  int array[n];
  printf("Recaman Sequence: \n");
  for(int i = 0; i<n; i++){
    if(i==0){
      array[i+1] = 1;
    }
    else if(array[i]%i ==0){
      array[i+1] = array[i]/i;
    }
    else{
      array[i+1] = array[i]*i;
    }
    printf("%d\n", array[i+1]);
  }
  printf("END\n");
}

int main(int argc, char** argv){
  int N = atoi(argv[1]);
  pid_t pid = fork();
  if(pid ==0){
    reca(N);
     return 0;
  }
  wait(NULL);
  return 0;
}
