#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

void *helloThread( int *n) {
   printf("Hello world! I am thread %p\n",n);
   pthread_exit(0); 
}
void main(int argc, char *argv[]){
  int num = atoi(argv[1]); 
  pthread_t tids[num]; 
  for (int i = 0; i < num; i++){ 
    pthread_create(&tids[i], NULL, helloThread,(void*)&tids[i]); 
    printf("Creating Thread %p\n",(int*)tids[i]); 
  }
  for (int j = 0; j < num; j++){
    pthread_join(tids[j],NULL); 
  }
}
