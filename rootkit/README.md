one of my red team tools, if you want to see
more of my red team tools, please email me mikeyjar1@gmail.com

This module works on Ubuntu 16. I use this so I only need to maintain
user land persistence on a target. It overwrites the kill command, 
giving any user root priviledges, and it hides from ps and lsmod
I have been also working on this to put etc/shadow and etc/passwd
into a webserver at specific times throughout the day so an attacker
can crack the passwords.

usage:

make
sudo insmod rk.ko

in the wild, you'd want to name this something not suspicous and 
change the date of the file

You would also need to make it insert on boot, and store the file
in a place a blue teamer may not look. 
