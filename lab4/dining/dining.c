#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
 
#define NUMPHILOSOPERS 5
#define THINKING 2
#define HUNGRY 1
#define EATING 0
#define LEFT (phnum + 4) % NUMPHILOSOPERS
#define RIGHT (phnum + 1) % NUMPHILOSOPERS

int state[NUMPHILOSOPERS];
int phil[NUMPHILOSOPERS] = { 0, 1, 2, 3, 4 };
sem_t lock;
sem_t S[NUMPHILOSOPERS];

void test(int phnum)
{
    if (state[phnum] == HUNGRY
        && state[LEFT] != EATING
        && state[RIGHT] != EATING) {
        state[phnum] = EATING;
        sleep(2);
        printf("Philosopher %d takes chopsticks %d and %d\n",
                      phnum + 1, LEFT + 1, phnum + 1);
        printf("Philosopher %d is eating\n", phnum + 1);
        sem_post(&S[phnum]);
    }
    sleep(1);
}
 
// pcik up chopsticks
void take_chopsticks(int phnum){
    sem_wait(&lock);
    state[phnum] = HUNGRY;
    printf("Philosopher %d is hungry\n", phnum + 1);
    test(phnum);
    sem_post(&lock);
    sem_wait(&S[phnum]);
    sleep(1);
}
 
// put down chopsticks
void put_chopsticks(int phnum){
 
    sem_wait(&lock);
    state[phnum] = THINKING;
    printf("Philosopher %d putting chopsticks %d and %d down\n",
           phnum + 1, LEFT + 1, phnum + 1);
    printf("Philosopher %d is thinking\n", phnum + 1);
    test(LEFT);
    test(RIGHT);	
    sem_post(&lock);
    
}

//control each philosopher
void* philospher(void* num){
  int full  = 0;
  int *i = num;
  while (1 && full < 3) {
    sleep(1);
    take_chopsticks(*i);
    full++;
    sleep(0);
    put_chopsticks(*i);
  }
  printf("Philosopher %d is done eating and has left the table\n", *i +1);
}
 
int main(){
    int i;
    pthread_t thread_id[NUMPHILOSOPERS];
    sem_init(&lock, 0, 1);
    for (i = 0; i < NUMPHILOSOPERS; i++)
        sem_init(&S[i], 0, 0);
    for (i = 0; i < NUMPHILOSOPERS; i++) {
      //create thread for each philosopher
        pthread_create(&thread_id[i], NULL,philospher, &phil[i]);
        printf("Philosopher %d has sat down at the table\nPhilosopher %d is thinking\n", i+1, i+1);
    }
    for (i = 0; i < NUMPHILOSOPERS; i++){
        pthread_join(thread_id[i], NULL);
    }
    printf("All philosophers have left the table\n");
}
