	global loader
	
	MAGIC_NUMBER equ 0x1BADB002
	FLAGS	     equ 0x0
	CHECKSUM     equ -MAGIC_NUMBER
	
	KERNEL_STACK_SIZE equ 4096
	
	section .bss
	align 4
	kernel_stack:
		resb KERNEL_STACK_SIZE
		
	section .text
	align 4
		dd MAGIC_NUMBER	
		dd FLAGS
		dd CHECKSUM
	loader:
		mov esp, kernel_stack + KERNEL_STACK_SIZE
		
		extern sum_of_three
		push dword 3
		push dword 2
		push dword 1
		call sum_of_three
		
		extern fb_write_cell
		push dword 0
		push dword 0
		push dword ' '
		push dword 0
		call fb_write_cell
		
	.loop:
		jmp .loop
