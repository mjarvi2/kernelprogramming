#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#define NUMACCOUNTS 3

//could't figure out how to get each account with different inital money
//and how to get the transfers to work, but I got a deposit to work
//of a fixed amount while trying to make a transfer method
pthread_mutex_t mutex;
int thread_id[NUMACCOUNTS]  = {0,1,2};
int balances[NUMACCOUNTS] = {0,0,0};
int i, j;

void * deposit(int *id){
  int deposit;
  while(j < NUMACCOUNTS){
    if(*id == 0) deposit = 700;
    else if(*id == 1) deposit = 800;
    else deposit = 600;
      balances[*id]= balances[*id]+ deposit;
      printf("Account %c depositing: %d balance is: %d\n", *id+65, deposit, balances[*id]);
      fflush(stdout);
      j++;
      if (rand()%100 >= 50)
	sleep(rand()%2);
    }
}
void * transfer(int *id){
  int transfer;
  if(*id == 0) transfer = 200;
  if(*id == 1) transfer = 100;
  if (balances[*id] - transfer<0) printf("insufficient funds to transfer\n");
      else{
	  balances[*id] = balances[*id]-  transfer;
	  balances[*id+1] = balances[*id +1] + transfer;
	  printf("Account %c depositing %d into Account %c\n",*id+65,transfer, *id+66);
	  printf("Account %c now has: %d\n", *id+65, balances[*id]);
	  printf("Account %c now has: %d\n", *id+66, balances[*id+1]);
	   if (rand()%100 >= 50)
	   sleep(rand()%2);
	}
  
  
}
void * withdraw(int *id){
  int with;
      with = 525;
      if (balances[*id] - with <0) printf("insufficient funds to withdraw\n");
      else{
	  balances[*id] = balances[*id]-  with;
	  printf("Account %c withdrawing: %d balance %d\n",*id+65, with, balances[*id]);
	}
      fflush(stdout);
      i++;
      if (rand()%100 >= 50){
	sleep(rand()%2);
    }
}

int main(){
  int i;
  pthread_t thread[NUMACCOUNTS];
  for(i = 0; i<NUMACCOUNTS;i++){
    pthread_create(&thread[i], NULL, (void *)deposit, &thread_id[i]);
  }
  for(i = 0; i<NUMACCOUNTS-1;i++){
    pthread_create(&thread[i], NULL, (void *)transfer, &thread_id[i]);
  }
  pthread_create(&thread[2],NULL, (void *)withdraw, &thread_id[2]);
  
  for(i=0; i< NUMACCOUNTS; i++){
      pthread_join(thread[i], NULL);
  }
  return 0;
}
